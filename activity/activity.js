// 1. What directive is used by Node.js in loading the modules it needs?

//  Answer: The "require" directive is used to load modules.

// 2. What Node.js module contains a method for server creation?

// Answer: The http module in Node.js contains methods for creating an HTTP server

// 3. What is the method of the http object responsible for creating a server using Node.js?

// Answer: The createServer method of the http object in Node.js is used to create an HTTP server.

// 4. What method of the response object allows us to set status codes and content types?
	
// Answer: The response.writeHead method of the response object in Node.js allows you to set the status code and content type of an HTTP response.

// 5. Where will console.log() output its contents when run in Node.js?

// Answer: When you run a Node.js program and use console.log() to print output, the output will be printed to the console where you are running the Node.js program. If you are running the Node.js program from a terminal window, the output from console.log() will be printed to the terminal window.

// 6. What property of the request object contains the address's endpoint?

// Answer: The request.url property of the request object in Node.js contains the endpoint of the request URL.

